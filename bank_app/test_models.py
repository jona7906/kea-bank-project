from django.test import TestCase
from django.contrib.auth.models import User
from .models import Customer, Account, Rank, Ledger, Bank, Transaction
import uuid
from.utils import get_ip

class CustomerModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create bank
        Bank.objects.create(name='Test Bank', ip_address=get_ip())
        # Create ranks
        rank_1 = Rank.objects.create(name='Gold', value="75")
        rank_2 = Rank.objects.create(name='Silver', value="50")
        # USER 1d
        user_1 = User.objects.create(username='john', first_name='John', last_name='Doe', email='john@example.com')
        customer_1 = Customer.objects.create(user=user_1, rank=rank_1, phone='1234567890')
        Account.objects.create(customer=customer_1, title='Account 1', is_loan=False)
        Account.objects.create(customer=customer_1, title='Loan 1', is_loan=True)
        # USER 2
        user_2 = User.objects.create(username='jane', first_name='Jane', last_name='Doe', email='jane@example.com')
        customer_2 = Customer.objects.create(user=user_2, rank=rank_2, phone='1234567890')
        Account.objects.create(customer=customer_2, title='Account 2', is_loan=False)

    def test_create_customer(self):
        customer_1 = Customer.objects.get(user__username='john')
        self.assertEqual(customer_1.user.username, 'john')
        self.assertEqual(customer_1.rank.name, 'Gold')
        self.assertEqual(customer_1.phone, '1234567890')
        # assert phone number max length is 15 characters
        self.assertLessEqual(len(customer_1.phone), 15)

    # test rank 1 is higher than rank 2
    def test_rank(self):
        customer_1 = Customer.objects.get(user__username='john')
        customer_2 = Customer.objects.get(user__username='jane')
        self.assertGreater(customer_1.rank.value, customer_2.rank.value)

    def test_customer_accounts(self):
        customer_1 = Customer.objects.get(user__username='john')
        customer_2 = Customer.objects.get(user__username='jane')
        # assert customers has account.balance 0
        self.assertEqual(customer_1.accounts.first().balance, 0)
        self.assertEqual(customer_2.accounts.first().balance, 0)

    def test_loan_to_account_transfer(self):
        customer_1 = Customer.objects.get(user__username='john')
        loan_1 = Account.objects.get(customer=customer_1, title='Loan 1')
        account_1 = Account.objects.get(customer=customer_1, title='Account 1', is_loan=False)
        bank_ip = get_ip()
        bank = Bank.objects.get(ip_address=bank_ip)
        uid = uuid.uuid4()

        # Initial balances before transfer
        initial_balance_loan = loan_1.balance
        initial_balance_account = account_1.balance

        # Transfer 100 from loan_1 to account_1
        transfer_amount = 100
        Ledger.transfer(transfer_amount, loan_1, "debit_text", account_1, "credit_text", uid, bank, is_loan=True)

        # Check updated balances after transfer
        updated_balance_loan = loan_1.balance
        updated_balance_account = account_1.balance

         # Assert transfer was successful
        self.assertEqual(updated_balance_loan, initial_balance_loan - transfer_amount)
        self.assertEqual(updated_balance_account, initial_balance_account + transfer_amount)


    def test_negative_transfer_amount(self):
        customer_1 = Customer.objects.get(user__username='john')
        loan_1 = Account.objects.get(customer=customer_1, title='Loan 1')
        account_1 = Account.objects.get(title='Account 1', customer=customer_1)
        bank_ip = get_ip()
        bank = Bank.objects.get(ip_address=bank_ip)
        uid = uuid.uuid4()

        # Initial balances before transfer
        initial_balance_loan = loan_1.balance
        initial_balance_account = account_1.balance

        # Test negative transfer amount
        negative_transfer_amount = -100

        # Assert transfer was unsuccessful and recieves AssertionsError
        with self.assertRaises(AssertionError):
            Ledger.transfer(negative_transfer_amount, loan_1, "debit_text", account_1, "credit_text", uid, bank, is_loan=True)

        # Assert balances are unchanged
        self.assertEqual(loan_1.balance, initial_balance_loan)
        self.assertEqual(account_1.balance, initial_balance_account)


    def test_insufficient_funds(self):
        customer_1 = Customer.objects.get(user__username='john')
        customer_2 = Customer.objects.get(user__username='jane')
        account_1 = Account.objects.get(customer=customer_1, title='Account 1')
        account_2 = Account.objects.get(customer=customer_2, title='Account 2')
        bank_ip = get_ip()
        bank = Bank.objects.get(ip_address=bank_ip)
        uid = uuid.uuid4()

        # Initial balances before transfer
        initial_balance_account_1 = account_1.balance
        initial_balance_account_2 = account_2.balance

        # Transfer amount greater than account balance
        transfer_amount = account_1.balance + 100

        with self.assertRaises(ValueError):
            Ledger.transfer(transfer_amount, account_1, "debit_text", account_2, "credit_text", uid, bank, is_loan=False)

        # Assert balances are unchanged
        self.assertEqual(account_1.balance, initial_balance_account_1)
        self.assertEqual(account_2.balance, initial_balance_account_2)
        