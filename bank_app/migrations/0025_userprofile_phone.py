# Generated by Django 4.1.9 on 2023-05-28 20:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bank_app', '0024_remove_userprofile_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='phone',
            field=models.CharField(db_index=True, default=None, max_length=20),
        ),
    ]
