# Generated by Django 4.1.9 on 2023-05-30 11:38

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('bank_app', '0024_alter_ledger_message'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountrequest',
            name='created_at',
            field=models.DateTimeField(db_index=True, default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='loanrequest',
            name='created_at',
            field=models.DateTimeField(db_index=True, default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='datetime',
            field=models.DateTimeField(db_index=True, default=django.utils.timezone.now),
        ),
    ]
