from django.contrib import admin

from .models import Customer, LoanRequest, AccountRequest, Ledger, Account, ApprovedLoanRequests, ApprovedAccountRequests, Rank, Bank, Transaction, UserProfile

admin.site.register(Customer) 
admin.site.register(LoanRequest) 
admin.site.register(AccountRequest) 
admin.site.register(Ledger) 
admin.site.register(Account) 
admin.site.register(ApprovedLoanRequests) 
admin.site.register(ApprovedAccountRequests) 
admin.site.register(Rank) 
admin.site.register(Bank) 
admin.site.register(Transaction) 
admin.site.register(UserProfile) 

