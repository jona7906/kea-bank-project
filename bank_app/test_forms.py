from django.contrib.auth.models import User
from django.test import TestCase
from .forms import TransferForm
from .models import Account, Customer, Rank, Ledger, Bank
from .utils import get_ip
import uuid


class TransferFormTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        rank = Rank.objects.create(name='Gold', value=75)
        cls.bank = Bank.objects.create(name='Test Bank', ip_address=get_ip())
        cls.user = User.objects.create_user(username='testuser', password='password')
        cls.customer = Customer.objects.create(user=cls.user, rank=rank, phone='1234567890')
        cls.debit_account = Account.objects.create(customer=cls.customer, is_loan=False, title='Debit Account')
        cls.credit_account = Account.objects.create(customer=cls.customer, is_loan=False, title='Credit Account')
        cls.loan_account = Account.objects.create(customer=cls.customer, is_loan=True, title='Loan Account')
        cls.create_transfer(cls.loan_account, cls.debit_account, 500)

    @classmethod
    def create_transfer(cls, debit_account, credit_account, amount, is_loan=True):
        # Create Ledger entries for transfer between accounts
        Ledger.transfer(
            amount=amount,
            debit_account=debit_account,
            debit_text=f'{debit_account.title} transfer',
            credit_account=credit_account,
            credit_text=f'{credit_account.title} deposit',
            is_loan=is_loan,
            uuid = uuid.uuid4(),
            bank = cls.bank
        )

    def test_transfer_form_valid(self):
        form_data = {
            'amount': 500,
            'debit_account': self.debit_account.id,
            'debit_text': 'Debit',
            'credit_account': self.credit_account.id,
            'credit_text': 'Credit',
            'bank': self.bank
        }
        form = TransferForm(data=form_data, user=self.user)
        self.assertTrue(form.is_valid())

    def test_transfer_to_loan_account_valid(self):
        form_data = {
            'amount': 500,
            'debit_account': self.debit_account.id,
            'debit_text': 'Debit',
            'credit_account': self.loan_account.id,
            'credit_text': 'Credit',
            'bank': self.bank

        }
        form = TransferForm(data=form_data, user=self.user)
        self.assertTrue(form.is_valid())

    def test_transfer_to_invalid_credit_account(self):
        form_data = {
            'amount': 500,
            'debit_account': self.debit_account.id,
            'debit_text': 'Debit',
            'credit_account': 99999,
            'credit_text': 'Credit',
            'bank': self.bank
        }
        form = TransferForm(data=form_data, user=self.user)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['credit_account'], ['Credit account does not exist.'])

    def test_transfer_form_invalid_amount(self):
        amounts = [2000, 3000]
        for amount in amounts:
            with self.subTest(amount=amount):
                form_data = {
                    'amount': amount,
                    'debit_account': self.debit_account.id,
                    'debit_text': 'Debit',
                    'credit_account': self.credit_account.id,
                    'credit_text': 'Credit',
                    'bank': self.bank
                }
                form = TransferForm(data=form_data, user=self.user)
                self.assertFalse(form.is_valid(), msg=f'Amount {amount} should be invalid.')
                self.assertEqual(form.errors['amount'], ['Amount exceeds balance of selected account.'])

    def test_transfer_form_same_debit_and_credit_account(self):
        accounts = [self.credit_account.id, self.debit_account.id]
        for account in accounts:
            with self.subTest(account=account):
                form_data = {
                    'amount': 500,
                    'debit_account': account,
                    'debit_text': 'Debit',
                    'credit_account': account,
                    'credit_text': 'Credit',
                    'bank': self.bank
                }
                form = TransferForm(data=form_data, user=self.user)
                self.assertFalse(form.is_valid())
                self.assertEqual(form.errors['credit_account'], ['Can not transfer to the same account.'])

    def test_transfer_form_invalid_other_customers_loan_account(self):
        # Create another customer with a loan account
        other_user = User.objects.create_user(username='otheruser', password='password')
        other_customer = Customer.objects.create(user=other_user, rank=self.customer.rank, phone='0987654321')
        other_loan_account = Account.objects.create(customer=other_customer, is_loan=True, title='Other Loan Act')
        

        form_data = {
            'amount': 500,
            'debit_account': self.debit_account.id,
            'debit_text': 'Debit',
            'credit_account': other_loan_account.id,
            'credit_text': 'Credit',
            'bank': self.bank
        }
        form = TransferForm(data=form_data, user=self.user)
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['credit_account'], ['Can not transfer to other customer\'s loan account.'])

    def test_debit_account_is_current_users_account(self):
        # Create another customer with a loan account
        other_user = User.objects.create_user(username='otheruser', password='password')
        other_customer = Customer.objects.create(user=other_user, rank=self.customer.rank, phone='0987654321')
        other_loan_account = Account.objects.create(customer=other_customer, is_loan=True, title='Other Loan Act')

        form_data = {
            'amount': 500,
            'debit_account': other_loan_account.id,
            'debit_text': 'Debit',
            'credit_account': self.credit_account.id,
            'credit_text': 'Credit',
            'bank': self.bank
        }
        form = TransferForm(data=form_data, user=self.user)
        self.assertFalse(form.is_valid())
