from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import Account, Bank, Transaction, Ledger
from .serializers import AccountSerializer, TransferSerializer, TransactionSerializer
from django.db import IntegrityError
from rest_framework.renderers import JSONRenderer
import requests
from django.conf import settings


@api_view(['GET'])
def get_accounts(request):
    accounts = Account.objects.all()
    serializer = AccountSerializer(accounts, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_account(request, id):
    try:
        account = Account.objects.get(id=id)
        serializer = AccountSerializer(account)
        return Response(serializer.data)
    except Account.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
def transfer_out(request):
    print("request json: ", request.data)
    
    # validate the data and send response 400 if not valid
    serialized_transfer = TransferSerializer(data=request.data)
    serialized_transfer.is_valid(raise_exception=True)
    
    bank_to_ip = serialized_transfer.data.get('bank_to_ip')
    bank_from_ip = serialized_transfer.data.get('bank_from_ip')
    
    # Find entries in the database
    try:
        bank_from = Bank.objects.get(ip_address=bank_from_ip)
    except Bank.DoesNotExist:
        return Response(f'Bank IP address {bank_from_ip} not recognized', status=status.HTTP_404_NOT_FOUND)
    try:
        bank_to = Bank.objects.get(ip_address=bank_to_ip)
    except Bank.DoesNotExist:
        return Response(f'Bank IP address {bank_to_ip} not recognized', status=status.HTTP_404_NOT_FOUND)
    try:
        debit_account = Account.objects.get(id=serialized_transfer.data.get("debit_account"))
    except Account.DoesNotExist:
        return Response(f'Account {serialized_transfer.data.get("debit_account")} not recognized', status=status.HTTP_404_NOT_FOUND)
    try:
        credit_account = Account.objects.get(id=serialized_transfer.data.get("credit_account"))
    except Account.DoesNotExist:
        return Response(f'Account {serialized_transfer.data.get("credit_account")} not recognized', status=status.HTTP_404_NOT_FOUND)
    
    uid = serialized_transfer.data.get('uuid')
    amount = float(serialized_transfer.data.get('amount'))
    debit_text = serialized_transfer.data.get('debit_text')
    
    # data is valid, create the ledger
    try:
        transaction_uuid = Ledger.external_transfer(amount, debit_account, credit_account, debit_text, uid, bank_from, bank_to, transfer_out=False)
        # doublecheck if the transaction was successful in sender bank
        serialized_transaction = TransactionSerializer(data={"uuid":transaction_uuid})
        serialized_transaction.is_valid(raise_exception=True)
        data = JSONRenderer().render(serialized_transaction.data)
        response = requests.get(f'http://{bank_from_ip}{settings.PORT}/get_transaction/', data=data, headers={'Content-Type': 'application/json'})
        if response.status_code != 200:
            print(f"**** Response status: {response.status_code} -- Rolling back transaction")
            try:
                Transaction.rollback(transaction_uuid)
                return Response(f'Transaction not found in the sender bank, rolled back transaction', status=status.HTTP_404_NOT_FOUND)
            except IntegrityError:
                print("**** Rollback failed")
                return Response(f'Transaction rollback attempt failed', status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        print("**** Transaction found in sender bank, returning transaction uuid to client")
        body = {
            "transaction_uuid": transaction_uuid
        }
        return Response(body, status=status.HTTP_201_CREATED)
    except IntegrityError:
        Response(f'Something went wrong and the transaction failed', status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
def get_transaction(request):
    serialized_transaction = TransactionSerializer(data=request.data)
    serialized_transaction.is_valid(raise_exception=True)
    try:
        transaction = Transaction.objects.get(uuid=serialized_transaction.data.get('uuid'))
        serializer = TransactionSerializer(transaction)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except Transaction.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
