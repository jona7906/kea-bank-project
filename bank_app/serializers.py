from rest_framework import serializers
from bank_app.models import Account, Ledger, Transaction


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = '__all__'


class TransferSerializer(serializers.Serializer):
    uuid = serializers.UUIDField()
    bank_to_ip = serializers.IPAddressField()
    bank_from_ip = serializers.IPAddressField()
    amount = serializers.DecimalField(max_digits=25, decimal_places=2, min_value=0)
    credit_account = serializers.IntegerField()
    debit_account = serializers.IntegerField()
    debit_text = serializers.CharField(max_length=100)
    credit_text = serializers.CharField(max_length=100)


class LedgerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ledger
        fields = ['amount', 'message', 'account']


class TransactionSerializer(serializers.Serializer):
    uuid = serializers.UUIDField()
    