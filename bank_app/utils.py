import ipaddress
import requests
from decimal import Decimal
from django import template
import os


def get_ip():
    ip = requests.get("http://whatismyip.akamai.com").text
    return ip

def test_ip(ip):
    try:
        ipaddress.ip_address(ip)
        print(f"**** The IP address '{ip}' is valid.")
        return True
    except ValueError:
        print(f"**** The IP address '{ip}' is NOT valid")
        return False
    

def convert_currency(from_currency, to_currency):
    api_key = os.environ.get('CURRENCY_API_KEY')
    url = f"https://api.freecurrencyapi.com/v1/latest?apikey={api_key}&base_currency={from_currency}&currencies={to_currency}"
    response = requests.get(url)
    data = response.json()
    conversion_rate = data['data'][to_currency]
    conversion_rate = Decimal(str(conversion_rate))
    return conversion_rate

register = template.Library()
