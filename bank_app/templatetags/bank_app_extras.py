from django import template

register = template.Library()

@register.filter
def mul(value, arg):
    try:
        value = float(value)
        arg = float(arg)
    except (TypeError, ValueError):
        raise ValueError('Cannot multiply non-numeric values')
    return round(value * arg, 2)