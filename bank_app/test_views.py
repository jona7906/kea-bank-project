from django.urls import reverse
from django.test import TestCase
from django.contrib.auth.models import User
from bank_app.models import Rank, Bank, Account, Ledger, Customer
from bank_app.utils import get_ip
import uuid


class IndexViewTests(TestCase):
    def setUp(self):
        # Create ranks
        rank_1 = Rank.objects.create(name='Gold', value=75)
        # create bank admin:
        self.staff_user = User.objects.create_user('Staff', email='adam@admin.com', password='staffpass')
        self.staff_user.first_name = 'Staff'
        self.staff_user.last_name = 'Admin'
        self.staff_user.is_staff = True
        self.staff_user.save()
        # create bank customer:
        self.customer_user = User.objects.create_user('Testuser', email='testuser@email.com', password='testpass')
        self.customer_user.first_name = 'John'
        self.customer_user.last_name = 'Doe'
        self.customer_user.rank = rank_1
        self.customer_user.save()
        # create Customer object associated with customer_user
        self.customer = Customer.objects.create(user=self.customer_user, rank=rank_1)

    def test_index_view_redirects_to_customer_dashboard_for_non_staff(self):
        self.client.login(username='Testuser', password='testpass')
        response = self.client.get(reverse('bank_app:index'))
        self.assertRedirects(response, reverse('bank_app:customer_dashboard'))

    def test_index_view_redirects_to_admin_page_for_staff(self):
        self.client.login(username='Staff', password='staffpass')
        response = self.client.get(reverse('bank_app:index'))
        self.assertRedirects(response, reverse('bank_app:admin_page'))


class MakeTransferViewTests(TestCase):
    def setUp(self):
        # Create ranks
        rank_1 = Rank.objects.create(name='Gold', value=75)
        # create bank admin:
        self.bank = Bank.objects.create(name='Test Bank', ip_address=get_ip())
        self.staff_user = User.objects.create_user('Staff', email='adam@admin.com', password='staffpass')
        self.staff_user.first_name = 'Staff'
        self.staff_user.last_name = 'Admin'
        self.staff_user.is_staff = True
        self.staff_user.save()
        # create bank customer:
        self.customer_user = User.objects.create_user('Testuser', email='testuser@email.com', password='testpass')
        self.customer_user.first_name = 'John'
        self.customer_user.last_name = 'Doe'
        self.customer_user.rank = rank_1
        self.customer_user.save()
        # create Customer object associated with customer_user
        self.customer = Customer.objects.create(user=self.customer_user, rank=rank_1)

        # create Accounts
        self.debit_account = Account.objects.create(customer=self.customer, title='Debit Account', is_loan=False)
        self.loan_account = Account.objects.create(customer=self.customer, title='Loan Account', is_loan=True)
        self.credit_account = Account.objects.create(customer=self.customer, title='Credit Account', is_loan=False)

        # Put money on debit account
        transfer_amount = 100
        self.uid = uuid.uuid4()
        Ledger.transfer(transfer_amount, self.loan_account, "Debit", self.debit_account, "Credit", self.uid, self.bank, is_loan=True)

    def test_make_transfer_view_redirects_to_login_for_anonymous_users(self):
        response = self.client.get(reverse('bank_app:make_transfer'))
        self.assertRedirects(response, '/account/login/?next=/make_transfer/')

    def test_make_transfer_view_renders_correct_template_for_logged_in_users(self):
        self.client.login(username='Testuser', password='testpass')
        response = self.client.get(reverse('bank_app:make_transfer'))
        self.assertTemplateUsed(response, 'bank_app/make_transfer.html')

    def test_make_transfer_redirects_after_valid_form_submission(self):
        self.client.login(username='Testuser', password='testpass')
        form_data = {
            'amount': 100,
            'debit_account': self.debit_account.id,
            'debit_text': 'Debit',
            'credit_account': self.credit_account.id,
            'credit_text': 'Credit',
            'bank': self.bank.pk
        }
        response = self.client.post(reverse('bank_app:make_transfer'), data=form_data)
        self.assertRedirects(response, reverse('bank_app:customer_dashboard'))

    def test_make_transfer_view_invalid_form_submission_amount_greater_than_balance(self):
        self.client.login(username='Testuser', password='testpass')
        # Set amount higher than balance of debit account
        form_data = {
            'amount': self.debit_account.balance + 1,
            'debit_account': self.debit_account.id,
            'debit_text': 'Debit',
            'credit_account': self.credit_account.id,
            'credit_text': 'Credit',
            'bank': self.bank.pk
        }
        response = self.client.post(reverse('bank_app:make_transfer'), data=form_data)
        # Check that the form is not valid
        self.assertFalse(response.context['form'].is_valid())


def test_make_transfer_view_invalid_form_submission_credit_account_does_not_exist(self):
    self.client.login(username='Testuser', password='testpass')
    # Set credit_account to a non-existing account
    form_data = {
        'amount': 100,
        'debit_account': self.debit_account.id,
        'debit_text': 'Debit',
        'credit_account': 9999999,
        'credit_text': 'Credit',
        'bank': self.bank.pk
    }
    response = self.client.post(reverse('bank_app:make_transfer'), data=form_data)
    self.assertFalse(response.context['form'].is_valid())


def test_make_transfer_view_invalid_form_submission_debit_and_credit_accounts_are_same(self):
    self.client.login(username='Testuser', password='testpass')
    # Set debit_account and credit_account to the same value
    form_data = {
        'amount': 100,
        'debit_account': self.debit_account.id,
        'debit_text': 'Debit',
        'credit_account': self.debit_account.id,
        'credit_text': 'Credit',
        'bank': self.bank.pk
    }
    response = self.client.post(reverse('bank_app:make_transfer'), data=form_data)
    self.assertFalse(response.context['form'].is_valid())
