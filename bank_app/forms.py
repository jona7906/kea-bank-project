from django import forms
from .models import Customer, Account, Bank
from django.forms import ModelForm
from django.contrib.auth.models import User
from .utils import get_ip, test_ip
from django.shortcuts import get_object_or_404


class LoanRequestForm(forms.Form):

    # Ensure loan_amount is positive

    loan_amount = forms.DecimalField(min_value=0)

    def clean_loan_amount(self):
        loan_amount = self.cleaned_data['loan_amount']
        if loan_amount < 0:
            raise forms.ValidationError("Please enter a positive loan amount.")
        return loan_amount


class AccountRequestForm(forms.Form):
    customer = forms.ModelChoiceField(queryset=Customer.objects.all(), widget=forms.HiddenInput())


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ["username", "first_name", "last_name", "email"]


class CustomerForm(ModelForm):
    class Meta:
        model = Customer
        fields = ["phone", "rank"]


class AccountModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return f'{obj.id} - {obj.title} ({obj.balance} DKK)'


class TransferForm(forms.Form):
    amount = forms.DecimalField(max_digits=25, decimal_places=2)
    debit_account = AccountModelChoiceField(queryset=Account.objects.all())
    debit_text = forms.CharField()
    credit_account = forms.IntegerField(label="Recipient account number:")
    credit_text = forms.CharField()
    bank = forms.ModelChoiceField(queryset=Bank.objects.all())

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        customer = Customer.objects.get(user=user)
        self.fields['debit_account'].queryset = Account.objects.filter(customer=customer, is_loan=False)

    def clean(self):
        cleaned_data = super().clean()
        amount = cleaned_data.get('amount')
        debit_account = cleaned_data.get('debit_account')
        credit_account_id = cleaned_data.get('credit_account')
        bank_in = cleaned_data.get('bank')

        if amount and debit_account and amount > debit_account.balance:
            self.add_error('amount', 'Amount exceeds balance of selected account.')
        
        bank_out_ip = get_ip()
        bank_in_ip = get_object_or_404(Bank, pk=bank_in.pk).ip_address
        cleaned_data['bank_in_ip'] = bank_in_ip
        cleaned_data['bank_out_ip'] = bank_out_ip
        
        if test_ip(bank_out_ip):
            if bank_out_ip == bank_in_ip: # Internal transfer
                if credit_account_id:
                    try:
                        credit_account = Account.objects.get(id=credit_account_id)
                    except Account.DoesNotExist:
                        self.add_error('credit_account', 'Credit account does not exist.')
                    else:
                        if credit_account == debit_account:
                            self.add_error('credit_account', 'Can not transfer to the same account.')
                        elif credit_account.is_loan and credit_account.customer != debit_account.customer:
                            self.add_error('credit_account', 'Can not transfer to other customer\'s loan account.')
                        cleaned_data['credit_account'] = credit_account
        else:
            self.add_error('bank', 'Something went wrong, try again.')
        return cleaned_data


class CurrencyForm(forms.Form):
    CURRENCY_CHOICES = [
        ('DKK', 'DKK'),
        ('USD', 'USD'),
        ('EUR', 'EUR'),
    ]
    currency = forms.ChoiceField(choices=CURRENCY_CHOICES, label='View your balance in different currencies:', widget=forms.Select(attrs={'onchange': 'this.form.submit();'}))