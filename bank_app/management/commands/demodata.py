from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from bank_app.models import Rank, Customer, LoanRequest, AccountRequest, Bank, Account, UserProfile
from django.conf import settings
from bank_app.utils import get_ip, test_ip
from django.db import transaction
from secrets import token_urlsafe


class Command(BaseCommand):
    def handle(self, **options):
        # check if ranks already exist
        print('Provisioning with default customer ranks, banks and a bank user...')
        if not Rank.objects.all():
            with transaction.atomic():
                Rank(name="Basic", value=25).save()
                Rank(name="Silver", value=50).save()
                Rank(name="Gold", value=75).save()
        else:
            print('One or more ranks already exist, stopped provisioning')
        if not Bank.objects.all():
            with transaction.atomic():
                for i in settings.REMOTE_IP:
                    name = i['name']
                    ip_address = i['address']
                    Bank(name=name, ip_address=ip_address).save()
        else:
            print('One or more banks already exist, stopped provisioning')

        if not User.objects.filter(username='bank').exists():
            with transaction.atomic():
                password = token_urlsafe(16)
                bank_user = User.objects.create_user('bank', email='', password=password)
                bank_user.first_name = 'Bank'
                bank_ip = get_ip()
                if test_ip(bank_ip) and Bank.objects.filter(ip_address=bank_ip).exists():
                    bank_user.last_name = Bank.objects.get(ip_address=bank_ip).name
                else:
                    print('**** Bank IP address not found in settings.REMOTE_IP, using default name')
                    bank_user.last_name = 'Local'
                bank_user.is_staff = False
                bank_user.save()
                bank_customer = Customer(user=bank_user, rank=Rank.objects.get(pk=3), phone='0000000')
                bank_customer.save()
                bank_account = Account(customer=bank_customer, title='Bank account', is_loan=True)
                bank_account.save()
                #TODO: Remove log from production
                print(f'Created a bank user with Username: bank -- Password: {password} -- Bank account: {bank_account.pk}')

        if settings.DEBUG:
            print('DEBUD mode, adding demo data...')
            if not AccountRequest.objects.all():
                # create bank admin:
                bank_admin = User.objects.create_user('adam', email='adam@admin.com', password='adampass123')
                bank_admin.first_name = 'Adam'
                bank_admin.last_name = 'Administrator'
                bank_admin.is_staff = True
                bank_admin.is_superuser = True
                bank_admin.save()
                bank_admin_profile = UserProfile(user=bank_admin, phone='+4524603849')
                bank_admin_profile.save()
                # create bank customer:
                john_user = User.objects.create_user('john', email='john@john.com', password='johnpass123')
                john_user.first_name = 'John'
                john_user.last_name = 'Doe'
                john_user.save()
                john_customer = Customer(user=john_user, rank=Rank.objects.get(pk=3), phone='12345678')
                john_customer.save()
                # create loan requests for the customer:
                loan_requests = [
                    LoanRequest(customer=john_customer, loan_amount=1000),
                    LoanRequest(customer=john_customer, loan_amount=2000),
                    LoanRequest(customer=john_customer, loan_amount=3000),
                ]
                for loan_request in loan_requests:
                    loan_request.save()

                    # create account requests for the customer:
                    account_requests = [
                        AccountRequest(customer=john_customer),
                        AccountRequest(customer=john_customer),
                        AccountRequest(customer=john_customer),
                    ]
                    for account_request in account_requests:
                        account_request.save()

                    print('Demo data generated successfully.')
            else:
                print('Data already exists, skipped generating.')
