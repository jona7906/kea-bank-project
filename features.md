---
layout: default
title: Features
---


## Project Features
- Staff:
    - Can log in and log out 
    - Can can create a new customer 
    - Can see all customers
    - Can can create an account for customer
    - Can view all accounts
    - Can view all account balances
    - Can view all account movements
    - Can view loan request and choose to approve or deny them
    - Can view account request and cohose to approve or deny them
- Customers
    - Can see their full name or their username
    - Can see their rank
    - Can see an overview of all owned accouns and owned loan accouns
    - Can see a detailed view of each account including the account name, balance, and movements. 
    - Can apply for a new account
    - Can apply for loans and request an amount, but only if they have a rank higher than 'basic' (e.g. silver or gold)
    - Can transfer money between own accounts to own loan accounts, if the account has the needed funds 
    - Can transfer money to other customers accounts (not their loan accounts)
    - Can transfer moeny to accounts in other banks (not their loan accounts)
    - Customers can add 2-factor-authentification from their own account

- Extra features
    - Admins get notified through SMS when a customer applies for a loan or an account
    - Customers can view their balances in different currencies on their dashboard page