---
layout: default
title: Lambda Bank 
---

The Lambda Bank project is is developed by
- Anna Dalsgaard
- Filip Soudakov
- Jonas Rossen
- Simon Klejnstrup 

as the product for final exam of the courses 'Development Environments' and 'Backend with Python and Django' at [Copenhagen School of Design and Technology](https://kea.dk).

## Project description
Lambda Bank is a bank application that allows users to create accounts, deposit and withdraw money, and transfer money to other users. The application is built with Django and Django REST Framework and uses a PostgreSQL database. 

The application is containerized with Docker. The project also uses docker-compose to run the application, a ngnix web server and a PostgreSQL database in separate containers.