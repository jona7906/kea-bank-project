---
layout: default
title: Documentation
---
## Cloning the project
Clone the project from GitLab: [https://gitlab.com/jona7906/kea-bank-project](https://gitlab.com/jona7906/kea-bank-project)

## Prerequisites
- docker
- docker-compose
- Python >=3.9

## Python virtual environment
The project uses a Python virtual environment to manage dependencies.
### Creating a Python virtual environment
Create a Python virtual environment with the following command:

```console
$ python3 -m venv path/to/<environment-name>
```
Where `<environment-name>` is the name of the environment you want to create.

### Activating the Python virtual environment
Activate the Python virtual environment with the following command:

```console
$ source path/to/<environment-name>/bin/activate
```

### Installing dependencies
From the project root folder install dependencies with the following command:

```console
$ pip install -r requirements.txt
```
### Updating dependencies
To update dependencies, run the following command from the project root folder:

```console
$ pip freeze > requirements.txt
```

## Environment variables
The project uses environment variables to determine which settings file to use. The environment variable is called RTE and can be set to either dev, test or prod. 

Settings are located in env-files in the root project folder. Each environment needs to have its own env-file with a corresponding name, e.g. env-dev, env-test and env-prod. Only env-test is included in the repository as it is the one needed for running the pipeline in GitLab.

Contact the project owner to get env-variables needed to run the application in other environments. Then, create an env-file with the name env-`<env>`, where `<env>` is either dev, test or prod, and add the variables to it.

## Development
Run the project from the root project folder containing 'docker-compose.yml' with:

```console
$ RTE=dev docker-compose up
```
This will run the project with a development database and development settings.

The project will be available at [http://localhost:8000](http://localhost:8000)

On the first run, demodata will be imported to the database. 

## Testing
To run the project in test mode, run the following command from the root project folder:

```console
$ RTE=test docker-compose up
```
This will run the project with a test database and test settings. Unit tests will be run and the project will be run and in case of errors, the project will exit with an error code.

## Production
To run the project in production mode, run the following command from the root project folder:

```console
$ RTE=prod docker-compose up
```
This will run the project creating a production database and using production settings. No demodata will be imported, only provisioning the database with the necessary tables such as Rank and Bank.

## Demodata
In development mode, demodata will be imported to the database on the first run. This will create both a staff user with following credentials:
- username: adam
- password: adampass123

and a regular user with following credentials:
- username: john
- password: johnpass123

## Django admin page and superusers
User adam is also a superuser and has access to the admin panel at [http://localhost:8000/admin](http://localhost:8000/admin)

Here, you can add any kind of data to the database easily.

### Creating a superuser
To create a superuser, run the following command from the root project folder:

```console
$ RTE=<env> docker-compose exec app python manage.py createsuperuser
```
Where `<env>` is either dev, test or prod depending on which environment you want to create the superuser in. You will be prompted to enter a username, email and password.

## Running static tests
To run static tests such as linting and security checks, run the following command from the root project folder with an active Python virtual environment:

```console
$ ./static_tests.sh
```

## Updating the local Docker image
`docker-compose.yml` uses a remote image from GitLab registry which is **not** updated locally automatically which means that there might come issues with missing dependencies.

To update the local Docker image with an updated image from the registry, run the following commands from the root project folder:

```console
$ docker-compose down
$ docker image rm registry.gitlab.com/jona7906/kea-bank-project/kea-bank:latest
$ docker-compose up
```

## Migration conflicts
If you get a migration conflict when running the project, run the following command from the root project folder:

```console
RTE=<env> docker-compose up
RTE=<env> docker-compose exec app sh 
python manage.py makemigrations --merge
```
This will merge the conflicting migrations and you can then run the project again with `RTE=<env> docker-compose up`.

## Removing the database
To remove the developement database, run the following command from the root project folder:

```console
RTE=<env> docker-compose down
docker volume rm kea-bank-project_postgres_data
```	

## Running the pipeline
The pipeline is located in `.gitlab-ci.yml` and is run automatically when pushing to the master `dev` and `main` branches. 

The pipeline consists of the following stages:
- static: runs static tests such as linting and security checks
- docs: builds the documentation on GitLab Pages
- build: runs unit tests and updates the Docker image in the registry 

## Currency conversion
The bank uses a third party API to convert currencies. The API is called [FreecurrencyAPI](https://app.freecurrencyapi.com/dashboard) and is free to use. 

The API is limited to 5000 requests per month. If you reach the limit, you can create a new API key and replace the old one in the env-file.

Add to the env-file:
```console
CURRENCY_API_KEY=<your-API-key>
```